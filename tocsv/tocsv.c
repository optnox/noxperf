#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <argp.h>

#ifndef __USE_BSD
# define __USE_BSD	// This makes YCM shut up..
#endif

#include <dirent.h>

#include "list.h"

struct value_t
{
	char *value;
};

struct perf_data_t
{
	char *name;

	// list of value_t
	list *values;
};

void print_help();

void parse_dir(list *perflist, const char *path);

struct perf_data_t* perf_data_new();
void perf_data_free(struct perf_data_t *perf);

void read_perf(list *perflist, const char*);
struct perf_data_t* read_perf_file(FILE*);

void value_free(struct value_t*);

char* append_path(const char*, const char*);

bool should_parse_subdir(const char*);


int
main(int argc, char **argv)
{
	if (argc != 2) {
		print_help();
		return 0;
	}

	const char *dirpath = argv[1];
	DIR *dir = opendir(dirpath);
	struct dirent *ent;

	// The tcase_list (test-case list) contains lists of perf_data_t
	// (data from individual PerformanceMonitors)- which again holds
	// lists of value_t.
	list tcase_list;

	list_new(&tcase_list, sizeof(list), (freeFunction)list_destroy);

	if (!dir) {
		printf("Failed to open directory '%s'\n", dirpath);
		exit(1);
	}

	while ((ent = readdir(dir)) != NULL) {
		if (ent->d_type == DT_DIR) {
			if (!should_parse_subdir(ent->d_name))
				continue;

			char *subdir = append_path(dirpath, ent->d_name);

			list perf_list;
			list_new(&perf_list, sizeof(struct perf_data_t), (freeFunction)perf_data_free);
			parse_dir(&perf_list, subdir);
			list_append(&tcase_list, &perf_list);

			free(subdir);
		}
	}

	closedir(dir);

	list perf_list;

	while (list_iter_next(&tcase_list, &perf_list)) {
		int data_points = 0;
		printf("%s\n", perf_list.name);

		while (data_points < 100) {
			struct perf_data_t perf;

			if (data_points == 0) {
				while (list_iter_next(&perf_list, &perf)) {
					list_iter_reset(perf.values);
					printf("%s, ", perf.name);
				}
				list_iter_reset(&perf_list);
				printf("\n");
			}

			while (list_iter_next(&perf_list, &perf)) {
				struct value_t value;

				if (list_iter_next(perf.values, &value)) {
					printf("%s,", value.value);
				} else {
					printf("<noval>,");
				}
			}

			printf("\n");

			data_points++;
			list_iter_reset(&perf_list);
		}
	}

	return 0;
}


void
print_help()
{
	printf("tocsv - Turn performance data into CSV content.\n");
	printf("Usage:  ./tocsv <environment perf directory>\n");
	printf("        The argument directory *must* be an environment log directory.\n");
	printf("        Only data-segments with the frame-accumulative attribute are\n");
	printf("        included. Globally accumulative data and events are ignored.\n");
}


void
parse_dir(list *perflist, const char *dirpath)
{
	DIR *dir = opendir(dirpath);
	struct dirent *ent;

	if (dir)
		fputs("Parsing directory: ", stderr);
	else
		fputs("Failed to open directory: ", stderr);
	fputs(dirpath, stderr);
	fputs("\n", stderr);
	if (!dir)
		return;

	const char *dirname = strrchr(dirpath, '/') + 1;
	perflist->name = malloc(strlen(dirname) + 1);
	strcpy(perflist->name, dirname);

	while ((ent = readdir(dir)) != NULL) {
		if (ent->d_type == DT_REG) {
			char *filepath = append_path(dirpath, ent->d_name);

			read_perf(perflist, filepath);

			free(filepath);
		}
	}

	closedir(dir);
}

struct perf_data_t*
perf_data_new()
{
	struct perf_data_t *perf;

	perf = (struct perf_data_t*)malloc(sizeof(struct perf_data_t));

	perf->name = (char*)malloc(sizeof(char) * 80);
	memset(perf->name, 0, 80);

	perf->values = (list*)malloc(sizeof(list));
	list_new(perf->values, sizeof(struct value_t), (freeFunction)value_free);

	return perf;
}

void
perf_data_free(struct perf_data_t *perf)
{
	free(perf->name);
	list_destroy(perf->values);
	free(perf->values);
}


void
read_perf(list *perflist, const char *path)
{
	FILE *file = fopen(path, "r");
	if (!file) {
		printf("Unable to open file '%s' (errno %d)\n", path, errno);
		return;
	}

	// Seek to the DATA-header
	const int BUFLEN = 80;
	char buf[BUFLEN];
	char *bufptr = NULL;

	while ((bufptr = fgets(buf, BUFLEN, file)) != NULL && strcmp(buf, "DATA\n"))
		{};

	if (!bufptr) {
		printf("No 'DATA'-header found in file '%s'\n", path);
		fclose(file);
		return;
	}

	struct perf_data_t *pd;

	while ((pd = read_perf_file(file)) != NULL) {
		list_append(perflist, pd);
		free(pd);
	}

	fclose(file);
}

struct perf_data_t*
read_perf_file(FILE *file)
{
	const int BUFLEN = 128;
	char buf[BUFLEN];
	char *bufptr = NULL;
	struct perf_data_t *perf = NULL;

	while ((bufptr = fgets(buf, BUFLEN, file)) != NULL && buf[0] != '\n') {
		if (strlen(buf) == 0) {
			return perf;
		}

		if (buf[0] == '"') {
			if (perf) {
				printf("Invalid format - unexpected \".\n");
				exit(1);
			}

			// If this performance monitor is not of type "acc.frame", seek until the
			// next blank line.
			if (!strstr(buf, "acc.frame")) {
				while (buf[0] != '\n' && bufptr) {
					bufptr = fgets(buf, BUFLEN, file);
				}

				if (!bufptr)
					return NULL;

				// Restart the loop - the next outer fgets will retrieve the first line
				// of the next data-segment.
				continue;
			}

			perf = perf_data_new();

			char *end_quote = strstr(buf+1, "\"");
			if (!end_quote) {
				printf("Invalid format: expected terminating \" on line '%s'", buf);
				exit(1);
			}

			// Copy the name into the perf_data's name-string
			memcpy(perf->name, bufptr+1, end_quote-bufptr-1);
		} else {
			if (!perf) {
				printf("Invalid format: no header found\n");
				exit(1);
			}

			// This is most likely a data-point on the format:
			// "<frame no.> <value>"
			char *space = strstr(bufptr, " ");
			if (!space) {
				printf("Invalid format: no space found on data-point line '%s'\n", buf);
				exit(1);
			}

			// Copy the value of the data point. The value-string has a trailing newline,
			// which must be replaced by a NULL and added to the perf_data's list.
			int len = strlen(space);
			space[len - 1] = 0;
			space++;
			len--;

			struct value_t val;
			val.value = malloc(len);
			strcpy(val.value, space);

			list_append(perf->values, &val);
		}
	}

	return perf;
}


void
value_free(struct value_t *value)
{
	if (value->value)
		free(value->value);
}


char*
append_path(const char *seg1, const char *seg2)
{
	int seg1_len = strlen(seg1);
	int seg2_len = strlen(seg2);

	char *final_path = malloc(seg1_len + seg2_len + 2);
	memcpy(final_path, seg1, seg1_len);

	if (final_path[seg1_len-1] != '/') {
		final_path[seg1_len++] = '/';
	}

	memcpy(final_path + seg1_len, seg2, seg2_len);
	final_path[seg1_len + seg2_len] = 0;

	return final_path;
}

bool
should_parse_subdir(const char *dirname)
{
	// Parse everything except hidden files, "." and "..".
	return dirname[0] != '.';
}
